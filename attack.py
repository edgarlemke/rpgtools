#!/usr/bin/python3

def attack(hab, apt, d20, enemy_hab, enemy_def, enemy_def_dmg, res, diff, weapon):
    if d20 == 20:
        mul = 3
    else:
        mul = 1

    test = (hab + apt + d20) - (enemy_hab + enemy_def + res)
    pass_ = test >= diff
    print(f"TEST: {test} >= {diff} - {pass_}")

    dmg = ( ((hab + diff)*2 + weapon + (apt*3)) - (enemy_def_dmg + (res*3)) ) * mul
    print(f"DMG: {dmg}")
