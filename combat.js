class Combat {

	static objs = []

	constructor (alias, teams, dice_data)	{
		//console.log(dice_data)
		this.alias = alias
		this.teams = teams
		this.action_order = this.sort_action_order(dice_data)
		//console.log(this.action_order)

		this.slots = []
		for (var o in this.action_order)	{
			var pair = this.action_order[o]
			var char_obj = pair[0]
			this.slots.push({char_obj: char_obj, active: true, hidden: false})
		}
		this.cursor = 0

		Combat.objs.push(this)
	}

	/*
	 dice_data is an array of Objects, in the format {char_obj: new Char(), d20: dice(20)}.
	 It's used for input of manually rolled D20 dices.
	*/
	sort_action_order (dice_data) {
		//console.log(dice_data)

		var action_order = []
		function safe_add (order_index, char_obj, str) {
			if (action_order[order_index] === undefined) {
				action_order[order_index] = [ [char_obj, str] ]
			}
			else {
				action_order[order_index].push( [char_obj, str] )
			}
		}

		for (var t in this.teams)	{
			var team = this.teams[t]

			for (var ch in team)	{
				var char_obj = team[ch]
				//console.log(char_obj)

				// look for d20 value in dice_data
				var d20 = dice(20)
				for(var i in dice_data)	{
					var team_dice = dice_data[i]

					var break_ = false
					for (var t in team_dice) {
						var char_dice = team_dice[t]

						if (char_dice.char_obj.name != char_obj.name)	{
							continue
						}

						var d20 = Number(char_dice.d20)
						var break_ = true
						break
					}

					if (break_) {
						break
					}
				}

				var order_index = (char_obj.stats.Agility + d20) //* 1000
				var str = char_obj.stats.Agility + "+" + d20

				safe_add(order_index, char_obj, str)
			}
		}

		
		var draws = {}
		function update_draws () {
			draws = {}
			for (var i in action_order)	{
				var order = action_order[i]
				if (order.length > 1)	{
					draws[i] = order
				}
			}
		}
		update_draws()

		//console.log(draws)

		if (Object.keys(draws).length > 0) {
			var msg = "Draws: \n"
			for(var i = 0; i < Object.keys(draws).length; i++) {
				var x = draws[ Object.keys(draws)[i] ] 
				for (var j = 0; j < x.length; j++) {
					//console.log( x[j] )
					msg += x[j][0].name + " - " + x[j][1] + "\n"
				}
			}
			alert(msg)
			throw msg
		}

		/*
		//console.log(Object.keys(draws).length)
		//console.log(draws.length)

		// while there are draws, reiterate dice rolling and update to higher index
		while (Object.keys(draws).length > 0)	{
			//console.log(draws)
			for(var i in draws)	{
				var order = draws[i]
				//console.log("i: " + i)
				//console.log(order)

				for(var j = order.length - 1; j >= 0; j--)	{
					var pair = order[j]

					var str = order[j][1]
					var char_obj = order[j][0]

					//console.log(char_obj)
					var d20 = dice(20)

					var new_index = Number(i) - d20
					//console.log("new_index: " + new_index)

					// add to new index
					safe_add(new_index, char_obj, str)
					//console.log("at new index")
					//console.log(action_order[new_index])

					// remove from old index
					if (action_order[i] !== undefined)	{
						var index_ = action_order[i].indexOf(pair)
						if (index_ > -1)	{
							action_order[i].splice(index_, 1)
							if (action_order[i].length == 0)	{
								action_order.splice(i, 1)
							}
						}
						else
							throw new Error("What!")
					}
					//console.log("at old index")
					//console.log(action_order[i])
				}

			}
			update_draws()
			//console.log(draws.length)
		}
		*/

		//console.log(action_order)

		var new_action_order = []
		for (var k in action_order)	{
			//console.log(k)

			var pos = action_order[k]
			new_action_order.push(pos[0])
		}

		new_action_order.reverse()
		return new_action_order

	}

	act (action, args) {
		var char_obj = this.slots[this.cursor].char_obj
		//console.log(char_obj)

		args.combat_obj = this
		//console.log(args)
		var result = char_obj.act(action, args)

		if (args.target_char_obj !== undefined)	{
			var target_char_obj = args.target_char_obj
			//console.log("target")
			//console.log(target_char_obj)

			for (var s in this.slots)	{
				var slot = this.slots[s]
				if (slot.char_obj !== target_char_obj)	{
					continue
				}

				// turn active all targets with health > 0
				// turn inactive all targets with health == 0
				slot.active = (target_char_obj.health > 0)
				//console.log("enemy " + (slot.active?"ALIVE":"DEAD"))
			}
		}

	}
	
	skip () {
		this.cursor = this._get_next_active_slot()
	}

	_get_next_active_slot ()	{
		// test all slots from the next after the cursor to the end of slots
		for (var c = this.cursor+1; c < this.slots.length; c++)	{
			var slot = this.slots[c]
			if (! slot.active)	{
				continue
			}

			return c
		}
		// test all slots from the beginning of slots to the current cursor
		for (var c = 0; c <= this.cursor; c++)	{
			var slot = this.slots[c]
			if(! slot.active)	{
				continue
			}

			return c
		}

		throw new Error("No active slots!")
	}

	get_enemy (char_obj)	{
		if (this._last_enemies === undefined)	{
			this._last_enemies = {}
		}
		//console.log(this._last_enemies)

		function get_last_enemy	(combat_obj, index)	{
			// if already has a last enemy alive, return it
			if (combat_obj._last_enemies[index] !== undefined)	{
				var last_enemy = combat_obj._last_enemies[index]
				//console.log("last_enemy: " + last_enemy.name)
	
				// if last_enemy is hidden, look for it
				var last_enemy_index = combat_obj.action_order.indexOf(last_enemy)
				if (combat_obj.slots[last_enemy_index].hidden)	{
					//console.log("returning previous hidden enemy: " + last_enemy.name)
					return last_enemy
				}
				else	{
					// else, if it is alive, attack it
					if (last_enemy.health > 0)	{
						//console.log("returning previous alive enemy: " + last_enemy.name)
						return last_enemy
					}
					// if it is dead, move on to another enemy
					else	{
						//console.log("previous enemy is dead: " + last_enemy.name)
						return false
					}
				}
			}
			else {
				//console.log("no previous enemy")
				return false
			}

		}
		var index = this.action_order.indexOf(char_obj)
		//console.log("index: " + index)
	
		var last_enemy = get_last_enemy(this, index)

		//console.log("last_enemy")
		//console.log(last_enemy)
		if (last_enemy !== false)	{
			return last_enemy
		}

		//console.log(char_obj)
		
		// if there's no last enemy alive, find another enemy
		var team_hidden = []
		for (var t in this.teams)	{
			var team = this.teams[t]
			//console.log("team")
			//console.log(team)

			// if char not in the team, it's an enemy team
			var is_enemy_team = team.indexOf(char_obj) == -1
			//console.log("is_enemy_team: " + is_enemy_team)
			
			if (is_enemy_team)	{
				var all_hidden = false
				//var hidden = true
				var indexed = []

				//var ct = 0
				while (/*hidden*/ true)	{
					//console.log("ct: " + ct)
					//ct++

					// dice is used to randomly choose the enemy
					// this is the reason for the while loop as well
					var enemy_index = dice(team.length)-1

					var team_char_obj = team[enemy_index] 
					var slot_index = this.action_order.indexOf(team_char_obj)
					var slot = this.slots[slot_index]

					// if indexed has same length as team, all chars are hidden 
					// break out of while loop
					if (indexed.length == team.length)	{
					//	console.log("all chars are hidden")
					//	all_hidden = true
						break
					}

					// skip char that has already been checked as hidden or dead
					if (indexed.indexOf(enemy_index) > -1)	{
						//console.log("already indexed: " + enemy_index)
						continue
					}

					/*
					// skip all hidden enemies
					if (slot.hidden)	{
						console.log("skipping hidden enemy")
						indexed.push(enemy_index)
						continue
					}
					*/

					// skip all dead enemies
					if (team_char_obj.health == 0)	{
						//console.log("skipping dead enemy")
						indexed.push(enemy_index)
						continue
					}

					//hidden = slot.hidden

					//console.log(char_obj)
					//console.log(team_char_obj)

					// return the first enemy
					//console.log("!index: " + index)
					this._last_enemies[index] = team_char_obj
					return team_char_obj
				}

				// if all chars from team are hidden, go to the next team
				if (all_hidden)	{
				//	console.log("all hidden!")
					team_hidden.push(t)
					continue
				}

			}
		}

		//if (team_hidden.length == this.teams.length - 1)	{
		//	throw new Error("All chars from other teams are hidden!")
		//}

		console.log("deadend")
		console.log(this._last_enemies)
	}


	one_active_team () {
		var active_teams = this._get_active_teams()

		var ct = 0
		for (var t in active_teams)	{
			if (active_teams[t])	{
				ct++
			}
		}
		return ct == 1
	}

	_get_active_teams () {
		var active_teams = {}

		for (var t in this.teams)	{
			var team = this.teams[t]

			for (var ch in team)	{
				var char_obj = team[ch]

				for (var sl in this.slots)	{
					var slot = this.slots[sl]
					if (slot.char_obj !== char_obj)	{
						continue
					}

					if (slot.active)	{
						active_teams[t] = true
					}
				}
			}

			if (active_teams[t] === undefined)	{
				active_teams[t] = false
			}
		}

		return active_teams
	}


	get_slot_index (char_obj) {
		for (var i = 0; i < this.slots.length; i++) {
			var slot = this.slots[i]
			if (slot.char_obj == char_obj)
				return i
		}
		return false
	}

	get_cursor_team () {
		var char_obj = this.slots[this.cursor].char_obj
		//console.log(char_obj)

		for (var i = 0; i < this.teams.length; i++) {
			var team = this.teams[i]
			//console.log(team)

			for (var j = 0; j < team.length; j++) {
				var tc = team[j]
				if (char_obj == tc) {
					return i
				}
			}
		}

		return false
	}

	get_cursor_enemy_teams () {
		var team = "" + this.get_cursor_team() + ""
		//console.log(typeof(team) + " " + team )

		var all_teams = Object.keys(this.teams)
		//console.log(all_teams)

		var i = all_teams.indexOf(team)
		//console.log(i)

		all_teams.splice(i, 1)
		//console.log(all_teams)
		
		var all_teams = all_teams.map(n => Number(n))

		return all_teams
	}

	get_hidden_enemies () {
		var hidden_enemies = []

		var enemy_teams = this.get_cursor_enemy_teams()
		//console.log(enemy_teams)

		for (var i = 0; i < enemy_teams.length; i++) {
			var enemy_team = enemy_teams[i]
			//console.log(enemy_team)

			var enemy_team_array = this.teams[enemy_team]
			//console.log(enemy_team_array)
			

			for (var j = 0; j < enemy_team_array.length; j++) {
				var enemy = enemy_team_array[j]
				//console.log(enemy)

				
				var slot = this.slots[ this.get_slot_index(enemy) ]
				//console.log(slot)
				if (slot.hidden) {
					hidden_enemies.push(enemy)
				}
				
			}
			
		}

		return hidden_enemies
	}

	get_attackable_enemies () {
		var enemies = []

		var enemy_teams = this.get_cursor_enemy_teams()
		//console.log(enemy_teams)
		for (var i = 0; i < enemy_teams.length; i++) {
			var team_index = Number( enemy_teams[i] )
			var team = this.teams[team_index]
			//console.log(team)

			for (var j = 0; j < team.length; j++) {
				var char_obj = team[j]
				var slot_index = this.get_slot_index(char_obj)
				var slot = this.slots[slot_index]

				//console.log(slot)

				if (slot.active == false || slot.hidden == true) {
					continue
				}

				enemies.push( Char.get_index(char_obj) )
			}
		}

		return enemies
	}


	static get_index (combat_obj) {
		for(var i = 0; i < Combat.objs.length; i++) {
			if ( Combat.objs[i] === combat_obj )
				return i
		}
		return false
	}
}
