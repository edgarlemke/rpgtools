var drop_pool = []

function flush_drop_pool () {
	drop_pool = []
}

/*
function airdrop () {
	var class_ =  item_classes[ dice(item_classes.length) - 1 ]

	var stat_attrs = []
	for(var i in new Stats()){
		stat_attrs.push(i)
	}

	var new_item = false
	var args = {}
	if(class_ == Ring || class_ == Gem)	{
		args.stat = stat_attrs[ dice(stat_attrs.length) - 1 ]
		new_item = new class_(args)
	}
	else {
		new_item = new class_()
	}

	drop_pool.push(new_item)

	return new_item
}


class AbsItem {
	static objs = []

	constructor () {
		if (this.constructor == "AbsItem") {
			throw new Error("AbsItem can't be instantiated")
		}

		AbsItem.objs.push(this)
	}
}
// NOTE: concrete classes only
var item_classes = []

class AbsWeapon extends AbsItem {
	static objs = []

	constructor () {
		super()

		if (this.constructor == "AbsWeapon") {
			throw new Error("AbsWeapon can't be instantiated")
		}

		AbsWeapon.objs.push(this)
	}
}

class AbsProtection extends AbsItem {
	static objs = []

	constructor () {
		super()

		if (this.constructor == "AbsProtection") {
			throw new Error("AbsProtection can't be instantiated")
		}

		AbsProtection.objs.push(this)
	}
}
*/


var ii = []
function I (
	name,
	category,
	weight,
	stacking,
	buildable,
	made_of,
	price,
	level,
	use_class,
	build_class,
	equip_slot
	)	{
	var obj = {
		"name": name,
		"category": category,
		"weight": weight,
		"stacking": stacking,
		"buildable": buildable,
		"made_of": made_of,
		"price": price,
		"level": level,
		"use_class": use_class,
		"build_class": build_class,
		"equip_slot": equip_slot,
		"meta_obj" : null
	}
	ii.push(obj)
	return obj
}


function find_item_obj (name) {
	for (var i in ii) {
		var obj = ii[i]
		if (obj.name == name) {
			return obj
		}
	}
	return false
}


function get_weight_price (item_obj) {
	var weight = 0
	var price = 0

	var made_of = item_obj.made_of
	for (var i in made_of) {
		// made of item
		var made_of_item = made_of[i]

		// find item object
		var target_item_obj = find_item_obj(i)

		for(var j = 0; j < made_of_item; j++) {
			weight += target_item_obj.weight
			price += target_item_obj.price
		}
	}

	item_obj.weight = weight
	item_obj.price = price * 2
}
