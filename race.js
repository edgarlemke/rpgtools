class Race {
	static objs = []

	constructor (name, stats, classless, actions) {
		this.name = name
		this.stats = stats
		this.classless = classless
		this.actions = actions

		Race.objs.push(this)
	}

	static is_valid (name) {
		return true
		//return _is_valid(Race.objs, name)
	}

	static get_obj (name) {
		for (key in Race.objs) {
			if (Race.objs[key].name == name)
				return Race.objs[key]
		}
		return false
	}

	static _last_races = []
	static get_random () {
		// remove some races when last races have all races
		if (Race._last_races.length == Race.objs.length) {
			var factor = Math.floor(Race.objs.length/3)
			var offset = dice(factor)-1
			var qt = dice(factor)
			Race._last_races.splice(offset,qt)
		}

		while (true) {
			var race = Race.objs[ dice(Race.objs.length)-1 ].name
			if (Race._last_races.indexOf(race) == -1) {
				break
			}
		}

		Race._last_races.push(race)
		return race
	}
}

