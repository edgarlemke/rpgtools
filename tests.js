/*
 * Used to test the engine functions to make sure all of them work. :)
 */


/* char.js */
//var random_char = Char.get_random()
//console.log(random_char)

//var avl_actions = random_char.get_available_actions()
//console.log(avl_actions)


/* combat.js */
var karma = Karma.get_random()
var stats = Stats.get_random()

var all_turns = []
var all_rounds = []

for(var x = 0; x < 100; x++)	{
	//console.log(JSON.stringify(stats))

	var player1teamA = new Char("player1teamA", 20, "Dwarf", "Fighter", JSON.parse(JSON.stringify(stats)), JSON.parse(JSON.stringify(karma)), ["Chaos"]) 
	var player2teamA = new Char("player2teamA", 20, "Dwarf", "Fighter", JSON.parse(JSON.stringify(stats)), JSON.parse(JSON.stringify(karma)), ["Chaos"]) 
	
	var player1teamB = new Char("player1teamB", 20, "Dwarf", "Fighter", JSON.parse(JSON.stringify(stats)), JSON.parse(JSON.stringify(karma)), ["Chaos"]) 
	var player2teamB = new Char("player2teamB", 20, "Dwarf", "Fighter", JSON.parse(JSON.stringify(stats)), JSON.parse(JSON.stringify(karma)), ["Chaos"]) 
	
	var teams = [ [player1teamA, player2teamA], [player1teamB, player2teamB] ]
	var combat = new Combat(teams)
	//console.log(combat)
	
	var turns = 0
	while ((! combat.one_active_team()) && turns < 100)	{
		var actions = ["punch", "kick", "kneestrike", "weakspot", "run_to_mama"]

		var next_player = combat.action_order[ combat.cursor ]
		//console.log(next_player)
		var next_index = combat.action_order.indexOf(next_player)

		if (combat.slots[next_index].hidden)	{
			var action = "show_up"
			combat.act(action, {char_obj: next_player})
		}
		else	{
			var enemy = combat.get_enemy(next_player)
			if (enemy === undefined)	{
				throw new Error("enemy is undefined")
			}
			//console.log(enemy)

			var enemy_index = combat.action_order.indexOf(enemy)
			if (enemy_index > -1 && combat.slots[enemy_index].hidden)	{
				var action = "seek"
			}
			else	{
				var action = actions[ dice(actions.length)-1 ]
			}

			combat.act(action, {char_obj: next_player, target_char_obj: enemy, attack_d20: dice(20), defense_d20: dice(20)})
		}
	
	
		turns++
	}
	
	var rounds = Math.ceil(turns/combat.slots.length)
	
	//console.log("TURNS: " + turns)
	//console.log("ROUNDS: " + rounds)
	all_turns.push(turns)
	all_rounds.push(rounds)
}
console.log(all_turns)
console.log(all_rounds)

var min_rounds = 0
var max_rounds = 0
var avg_rounds = 0
for(var i=0;i<all_rounds.length;i++){
	avg_rounds += all_rounds[i]
	if(all_rounds[i]>max_rounds)
		max_rounds=all_rounds[i]
	if(min_rounds==0)
		min_rounds=all_rounds[i]
	else if(all_rounds[i]<min_rounds)
		min_rounds=all_rounds[i]
}
var avg_rounds = avg_rounds / all_rounds.length
console.log(avg_rounds)
console.log(max_rounds)
console.log(min_rounds)
