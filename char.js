class Char {
	static objs = []
	static motivations = []
	static actions = []

	static is_valid_motivation(m) {
		return (Char.motivations.indexOf(m) > -1)
	}

	constructor (name, age, race, class_, player_stats, karma_pts= false, motivations= [], health= 0, xp= 0) {
		// check errors
		if (! Race.is_valid(race) ) {
			throw "Raça inválida: " + race
		}
		if (Race.get_obj(race).classless !== true && (!Class.is_valid(class_)) ) {
			throw "Classe inválida: " + class_
		}
		if ( karma_pts === false ) {
			throw "Traços não fornecidos!"
		}
		for (var key in motivations) {
			var m = motivations[key]
			if (! Char.is_valid_motivation(m) ) {
				throw "Motivação inválida: " + m
			}
		}
		

		// set vars
		this.name = name
		//console.log(this.name)
		this.age = age
		this.race = Race.get_obj(race)
		
		if (race.classless !== true) {
			this.class_ = Class.get_obj(class_)
		}
		else {
			this.class_ = null
		}

		this.motivations = motivations

		this.player_stats = player_stats
		this.stats = Object.assign({}, player_stats)

		// add default actions
		this.actions = Object.assign({}, Char.actions)
		// add race actions
		if (this.race.actions !== undefined) {
	    		for (var i in this.race.actions) {
				this.actions[i] = this.race.actions[i]
    			}
    		}
		// add class actions
		if (this.class_ !== undefined && this.class_.actions !== undefined) {
			for (var i in this.class_.actions) {
				this.actions[i] = this.class_.actions[i]
			}
		}
    
    		// merge race stats with stats
    		for (var t in this.race.stats) {
			this.stats[t] += this.race.stats[t]
		}

		// merge class_ stats with stats
		if (race.classless !== true) {
			for (var t in this.class_.stats) {
				this.stats[t] += this.class_.stats[t]
			}
		}

		/*
		// karma related
		//
		var sorted_karma_pts = Char.sort_karma_pts(karma_pts)
		var new_stats = sorted_karma_pts[0]

		var new_life = sorted_karma_pts[1]
		this.karma = sorted_karma_pts[2]
	
		// merge new karma stats with stats
		for (var t in new_stats) {
			this.stats[t] += new_stats[t]
		}
		

		this.health = 275 + health + new_life
		*/

		this.aptitudes = Object.assign({}, damage_obj)
		this.resistences = Object.assign({}, damage_obj)


		this.health = 275

		this.xp = xp


		this.inventory = new Inventory(this)
/*
		// all chars start with 160 coins
		for (var i = 0; i < 160; i++) {
			var c = new Coin()
			this.grab_item(c)
		}

		//console.log(this.inventory)
*/


		this.live = {
			stats: Object.assign({}, this.stats),
			health: this.health
		}


		Char.objs.push(this)
	}

	grab_item (item_obj) {
		//console.log( item_obj.constructor.weight + " - " + this.inventory.free_space )

		/*
		if( item_obj.constructor.weight > this.inventory.free_space ) {
			throw "Não há espaço no inventário para este item. Espaço: " + this.inventory.free_space + " Peso do item: " + item_obj.constructor.weight
		}
		*/

		this.inventory.items.push(item_obj)

		/*
		this.inventory.update_capacity()
		this.inventory.update_free_space()
		*/
	}

	drop_item (item_obj) { // TODO: use pop_item
		drop_pool.push( this.inventory.items.pop(item_obj) )
		var equipped = this.inventory.equipped_items.indexOf(item_obj) > -1
		if (equipped)	{
			this.inventory.unequip(item_obj)
		}

		/*
		this.inventory.update_capacity()
		this.inventory.update_free_space()
		*/
	}

	act (action, args) {
		var actions = this.get_available_actions()
		if (actions[action] === undefined)	{
			throw new Error("Unavailable action to the char right now: " + action)
		}

		//console.log(args)

		return actions[action](args)
	}

	get_available_actions () {
		return this.actions
	}

	add_action (action)	{
		if (this.class_.actions.indexOf(action) == -1)	{
			throw new Error("Invalid action to add for class " + this.class_.name)
		}

		this.actions.push(action)
	}
	
	get_bonus_pts (stats)	{
		var bonus_items = [Ring, Gem]
		var bonus_pts = 0
		for (var i = 0; i < this.inventory.equipped_items.length; i++)	{
			var item = this.inventory.equipped_items[i]

			// add bonus pts from bonus items
			if (
				(bonus_items.indexOf(item.constructor) > -1)
				&& (stats.indexOf(item.stat) > -1)
			)	{
				for (var j = 0; j < item.constructor.bonus_pts; j++)	{
					bonus_pts += 1
				}
				continue
			}
		}
		return bonus_pts
	}
	

	static get_random()	{
		var motivations = []
		for (var i = 0; i < 3; i++)	{
			var m = Char.motivations[ dice(Char.motivations.length) - 1 ]
			if(motivations.indexOf(m) > -1)	{
				i--
				continue
			}			
			motivations.push(m)
		}


		var make_class_fit = dice(2) == 2

		var race = Race.get_random()
		var race_obj = Race.get_obj(race)

		if (race_obj.classless == true) {
			var class_ = null
		}
		else {
			if (make_class_fit) {
				//console.log("Make class fit!")

				// get habilities with higher points
				//console.log(race_obj)
				var stats_keys = Object.keys(race_obj.stats)
				var max = 0
				var strongest_stats = []
				for (var h in stats_keys) {
					var hability = stats_keys[h]
					var hab_pts = race_obj.stats[hability]

					if (hab_pts > max) {
						max = hab_pts
						strongest_stats = [ hability ]
					}
					else if (hab_pts == max) {
						strongest_stats.push( hability )
					}
				}
				//console.log(race_obj.stats)
				//console.log(strongest_stats)

				// get classes with higher points on the race's habilities with higher points
				var max = 0
				var fittest_classes = []
				for (var cl in Class.objs) {
					var class__ = Class.objs[cl]
					var class_stats = class__.stats

					//console.log(class_stats)

					for (var st in strongest_stats) {
						var stat = strongest_stats[st]
						//console.log(stat)

						var new_max = class_stats[stat]
						//console.log(stat + " - " + new_max)
						if (class_stats[stat] > max) {
							max = class_stats[stat]
							fittest_classes = [class__]
						}
						else if (class_stats[stat] == max) {
							fittest_classes.push( class__ )
						}
					}
				}
				//console.log(fittest_classes)
				//for(var c in fittest_classes){
				//	var x= fittest_classes[c]
				//	console.log(x)
				//	console.log(x.stats)
				//	console.log(strongest_stats)
				//	console.log()
				//}

				// if more than one class, choose randomly
				var i = dice(fittest_classes.length) - 1
				var class_ = fittest_classes[i].name
			}
			else {
				var class_ = Class.get_random()
			}
		}
		//console.log(race)
		//console.log(class_)

		var new_stats = Object.assign( {}, race_obj.stats)
		//console.log(new_stats)

		if (class_ != undefined) {
			var class_obj = Class.get_obj(class_)

			var keys = Object.keys( class_obj.stats )
			for(var k in class_obj.stats) {
				var stat = class_obj.stats[k]
				//console.log(stat)
				new_stats[k] += stat
			}
		}
		//console.log(new_stats)
		
		var max = 0
		var max_stats = []
		for (var k in new_stats) {
			//console.log(k)
			var new_max = new_stats[k]
			if (new_max > max) {
				var max = new_max
				var max_stats = [k]
			}
			else if (new_max == max) {
				max_stats.push(k)
			}
		}

		//console.log(max_stats)
		//console.log(new_stats)

		var make_stats_fit = dice(2) == 2
		if (make_stats_fit) {
			//console.log("Make stats fit!")
			var player_stats = new Stats(0,0,0,0,0)

			// ALL players have 5 player points

			var player_stat_fitness = 2 + dice(3)
			for(var i = 0; i < player_stat_fitness; i++) {
				var hab = max_stats[ dice(max_stats.length) - 1 ]
				player_stats[hab] += 1
			}

			var remaining_points = 5 - player_stat_fitness
			if (remaining_points > 0) {
				var hab_keys = Object.keys(player_stats)
				for(var i = 0; i < remaining_points; i++) {
					var hab = hab_keys[ dice(5) - 1 ]
					player_stats[hab] += 1
				}
			}
		}
		else {
			var player_stats = Stats.get_random()
		}


		var new_char = new Char(
			Char.get_random_name(),
			17 + dice(100-17),
			race, //Race.get_random(),
			class_, //Class.get_random(),
			player_stats, //Stats.get_random(),
			[/*Karma.get_random(), Karma.get_random(), Karma.get_random()*/],
			motivations,
			0 //dice(4) * 10
		)
		
		var random_equip = Char.random_equip(new_char)

		return new_char
	}

	static get_random_name() {
		var names = ["Ash", "Zig", "Harley", "Jesse", "Kai", "Quin", "Remi",
			"Taylor", "Val", "Xerxes", "Xo", "Zane", "Sasha", "Dani",
			"Collie", "Ebany", "Adri", "Acha", "Lavon", "Ren", "Aldwin",
			"Farvardin", "Revon", "Darel", "Galel", "Dali", "Raven",
			"Easton", "Landry", "Atlas", "August", "Avery", "Cameron",
			"Colby", "Koda", "Saylor", "Rowan", "Kirby", "Maddox", "Roxxie",
			"Sydney", "Ellison", "Auden", "Jude", "Phoenix", "Loren",
			"Kai", "Lane", "Nowell", "Oakley", "Alex", "Oliver", "Jessie"]
		return names[dice(names.length)-1] + " " + names[dice(names.length)-1]
	}

	/*
	static sort_karma_pts (karma_pts) {
		var new_stats = new Stats(0,0,0,0,0)
		var new_life = 0
		var new_karma_pts = []

		var attrs = Object.keys(new_stats)
		attrs.push("Life")
		//console.log(attrs)

		var to_gen_karma_pts = karma_pts.length
		for (var i = 0; i < to_gen_karma_pts; i++) {

			var to_gen_attr = dice( attrs.length )

			// skip attributes chosen by the player, so karma points
			// affect only not chosen attributes
			var chosen_attr = false
			for(var j = 0; j < to_gen_karma_pts; j++) {
				var jkarma = karma_pts[j]
				if(jkarma.attr == to_gen_attr) {
					chosen_attr = true
					break
				}
			}
			if ( chosen_attr == true ) {
				i--
				continue
			}

			var karma = karma_pts[i]
			var gen_karma = new Karma(to_gen_attr, karma.value * -1)

			new_karma_pts.push(karma)
			new_karma_pts.push(gen_karma)

			var life_value = 25

			if (karma.attr >= 1 && karma.attr <= 5) {
				new_stats[ attrs[karma.attr-1] ] += karma.value
			}
			else {
				new_life += karma.value * life_value
			}

			if (gen_karma.attr >= 1 && gen_karma.attr <= 5) {
				new_stats[ attrs[gen_karma.attr-1] ] += gen_karma.value
			}
			else {
				new_life += gen_karma.value * life_value
			}
		}

		return [new_stats, new_life, new_karma_pts]
	}
	*/

	static random_equip (char_obj)	{
		var coins = 160
		var equipment = []

		var max = 0
		var habs = []
		for (var h in char_obj.stats) {
			var stat = char_obj.stats[h]
			if(stat > max) {
				max = stat
				habs = [h]
			}
			else if (stat == max) {
				habs.push(h)
			}
		}

		// get primary weapon
		var weapons = []
		for (var w in ww) {
			var weapon_obj = ww[w]

			for (var h in weapon_obj.habilities) {
				var hab = weapon_obj.habilities[h]
				
				for (var i in habs) {
					if (hab == habs[i]) {
						weapons.push(weapon_obj)
					}
				}
			}
		}

		var max = 0
		var max_damage_weapons = []
		for (var w in weapons) {
			var weapon_obj = weapons[w]

			if (weapon_obj.damage > max) {
				var max = weapon_obj.damage
				var max_damage_weapons = [weapon_obj]
			}
			else if (weapon_obj.damage == max) {
				max_damage_weapons.push(weapon_obj)
			}
		}

		// get primary weapon
		var primary_weapon = null
		while (true) {
			var primary_weapon = Object.assign({}, max_damage_weapons[ dice( max_damage_weapons.length) - 1 ])

			if (char_obj.class_ != false) {
				if (char_obj.class_.name == "Shooter") {
					if (primary_weapon.damage_type.indexOf("Shooting") == -1)	{
						continue
					}
				}
	
				if (char_obj.class_.name == "Cheeseperson") {
					if (primary_weapon.damage_type.indexOf("Cheese") ==  -1)	{
						continue
					}
				}

				if (char_obj.class_.name == "Demonologist")	{
					if (primary_weapon.damage_type.indexOf("Shadows") == -1)	{
						continue
					}
				}
			}

			break
		}
		primary_weapon.item_obj = Object.assign({}, primary_weapon.item_obj)
		primary_weapon.item_obj.meta_obj = primary_weapon

		char_obj.grab_item(primary_weapon.item_obj)
		char_obj.inventory.equip(primary_weapon.item_obj)
		coins -= primary_weapon.item_obj.price

		// get ammo, if primary weapon is of damage type "Shooting"
		var as = get_ammo_spending(primary_weapon)
		var ammo = as[1]
		for (var a in ammo) {
			char_obj.grab_item(ammo[a])
		}
		coins -= as[0]


		// get secondary weapon
		var secondary_weapon = null;
		for(var c= 0; c < (ww.length*10); c++) {	
			var secondary_weapon = ww[ dice(ww.length) - 1 ]
			if (secondary_weapon.price < coins)
				break
		}

		if (secondary_weapon == null)
			return

		secondary_weapon = Object.assign({}, secondary_weapon)
		secondary_weapon.item_obj = Object.assign({}, secondary_weapon.item_obj)
		secondary_weapon.item_obj.meta_obj = secondary_weapon

		char_obj.grab_item(secondary_weapon.item_obj)
		coins -= secondary_weapon.item_obj.price


		// get ammo for secondary weapon
		var as2 = get_ammo_spending(secondary_weapon)
		var ammo = as2[1]
		for(var a in ammo) {
			char_obj.grab_item(ammo[a])
		}
		coins -= as[0]


		var ct = 0
		while (true){
			var choice = dice(4)

			var product = null
			switch(choice) {
				case 1:
				case 2:
					product = Object.assign({}, pp[ dice(pp.length)-1 ])
					product.item_obj = Object.assign({}, product.item_obj)

					// skip Protection items the char already has
					var shift = false
					//console.log("--")
					for (var i = 0; i < char_obj.inventory.items.length; i++) {
						var item = char_obj.inventory.items[i]
						//console.log(item)
						/*
						if (item.item_obj === undefined) {
							continue
						}
						*/
						if (item.category != "Protection") {
							continue
						}

						//if (product.item_obj.name == item.item_obj.name) {
						if (product.item_obj.equip_slot == item.equip_slot) {
							var shift = true
							break
						} 
					}
					//console.log("--")
					if (shift)	{
						continue
					}

					break
				case 3:
					product = Object.assign({}, dd[ dice(dd.length)-1 ]) 
					product.item_obj = Object.assign({}, product.item_obj)
					break
				case 4:
					var maybe = [
						"StrengthRing", "AgilityRing", "DextrityRing", "IntelligenteRing", "CharismaRing",
						"StrengthGem", "AgilityGem", "DextrityGem", "IntelligenceGem", "CharismaGem"
					]
					product = Object.assign({}, find_item_obj(maybe[ dice(maybe)-1 ]) )
					break
			}

			if (choice == 1 || choice == 2 || choice == 3) {
				var item_obj = product.item_obj
				item_obj.meta_obj = product
			}
			else {
				var item_obj = product
			}


			if (coins > item_obj.price) {
				char_obj.grab_item(item_obj)
				
				if (item_obj.equip_slot !== null) {
					char_obj.inventory.equip(item_obj)
				}

				coins -= item_obj.price
			}
			else {
				if (ct == 100)
					break
				else
					ct += 1
			}
		}

		for (var c = 0; c < coins; c++) {
			var coin = Object.assign({}, find_item_obj("Coin"))
			char_obj.grab_item(coin)
		}

		return
	}


	static get_index (char_obj) {
		for(var i = 0; i < Char.objs.length; i++) {
			if (Char.objs[i] === char_obj)
				return i
		}
		return false
	}

}

class Karma {
//	static objs = []

	constructor (attr, value) {
		this.attr = attr
		this.value = value

//		Karma.objs.push(this)
	}

	static get_random () {
		return new Karma(dice(6), (dice(2) == 1 ? +1 : -1))
	}
}


class Inventory {
	static objs = []

	constructor (char_obj) {
		this.char_obj = char_obj
		this.items = []

		this.update_capacity()
		this.update_free_space()

		this.equipped_items = []
		
		Inventory.objs.push(this)
	}

	equip (item_obj)	{
		this.equipped_items.push(item_obj)
	}

	unequip (item_obj)	{
		var index = this.equipped_items.indexOf(item_obj)
		if (index > -1)	{
			this.equipped_items.splice(index, 1)
		}
		else	{
			throw new Error("Trying to unequip item not equipped before")
		}
	}

	pop_item (item_obj) {
		var popped = false
		var index = this.items.indexOf(item_obj)
		if (index > -1)	{
			var popped = this.items.splice(index, 1)
		}

		if(this.equipped_items.indexOf(item_obj) > -1)	{
			this.unequip(item_obj)
		}

		this.update_capacity()
		this.update_free_space()

		return popped
	}

	update_capacity () {
		var cap = this.char_obj.stats.Strength * 5
		if (cap < 5)
			cap = 5
		this.capacity = cap
	}

	update_free_space () {
		this.free_space = this.capacity

		for (var i in this.items) {
			var item = this.items[i]
			
			if ( item.constructor.stacking == 1 ) {
				this.free_space -= item.constructor.weight
			}
			else {
				this.free_space -= this._get_stacked_weight(i)
			}
		}
	}

	_get_stacked_weight (item_obj_index) {
		var weight = 0

		var filtered = []
		for (var i in this.items) {
			//console.log("i: " + i)
			var item = this.items[i]

			if (item.constructor != this.items[item_obj_index].constructor)
				continue

			filtered.push(item)

			if (i == item_obj_index)
				break
		}

		if ( ((filtered.length-1) % this.items[item_obj_index].constructor.stacking) == 0 ) {
			weight = this.items[item_obj_index].constructor.weight
		}

		return weight
	}

	get_weapon () {
		for (var i = 0; i < this.equipped_items.length; i++) {
			var item = this.equipped_items[i]
			if (item.category != "Weapon") {
				continue
			}

			return item
		}
		return false
	}

}
