defaults = {
	races : [
		{
			name: "Elf",
			stats: new Stats(0, 0, 2, 2, 1)
		},
		{
			name: "Human",
			stats: new Stats(0, 1, 1, 2, 1)
		},
		{
			name: "Yeti",
			stats: new Stats(3, 2, 0, 0, 0)
		},
		{
			name: "Dwarf",
			stats: new Stats(2, 1, 2, 0, 0)
		},
		{
			name: "Tiefling",
			stats: new Stats(2, 1, 1, 1, 0)
		},
		{
			name: "Gnome",
			stats: new Stats(0, 0, 0, 2, 3)
		},
		{
			name: "Orc",
			stats: new Stats(3, 1, 1, 0, 0)
		},
		{
			name: "Cyborg",
			stats: new Stats(1, 0, 1, 3, 0)
		},
		{
			name: "Reptilian",
			stats: new Stats(0, 3, 0, 2, 0)
		},
		{
			name: "Atlant",
			stats: new Stats(2, 0, 3, 0, 0)
		},
		{
			name: "Alien",
			stats: new Stats(0, 0, 1, 3, 1)
		},

		{
			name: "Arve",
			stats: new Stats(2, 0, 0, 1, 1)
		},


		{
			name: "AAA",
			stats: new Stats(0, 2, 0, 0, 3)
		},

		{
			name: "BBB",
			stats: new Stats(0, 3, 0, 0, 2)
		},

		{
			name: "CCC",
			stats: new Stats(1, 0, 1, 0, 3)
		},

		{
			name: "DDD",
			stats: new Stats(0, 2, 2, 0, 1)
		},


		{
			name: "Ghoul",
			stats: new Stats(3, 3, 2, 2, 0),
			classless: true,
			actions : {
				"SinisterFeast": sinister_feast,
			}
		},
		{
			name: "Cheeseperson",
			stats: new Stats(1, 1, 3, 2, 3),
			classless: true,
			actions: {
				"CheeseSpikes": cheese_spikes,
				"FieryCreamCheese": fiery_creamcheese,
			}
		}

	],
	classes: {
		fighter:	{
			name: "Fighter",
			stats: new Stats(3, 1, 1, 0, 0),
			actions: {
				"ViolentOnslaught": violent_onslaught,
				"BloodyJump": bloody_jump,
			}

		},
		swordsman:	{
			name: "Swordsman",
			stats: new Stats(1, 1, 3, 0, 0),
			actions : {
				"ShockBlade": shock_blade,
				"BloodyJump": bloody_jump,
			}
		},
		shooter:	{
			name: "Shooter",
			stats: new Stats(0, 2, 3, 0, 0),
		},
		engineer:	{
			name: "Engineer",
			stats: new Stats(0, 0, 2, 3, 0)
		},
		witch:	{
			name: "Witch",
			stats: new Stats(0, 0, 0, 3, 2),
			actions : {
				"AstralDischarge": astral_discharge,
			}
		},
		alchemist:	{
			name: "Alchemist",
			stats: new Stats(0, 0, 2, 3, 0),
			actions : {
				"ElementalSphere": elemental_sphere,
				"ElementalSpikes": elemental_spikes,
				"ElementalSpiral": elemental_spiral,
				"ElementalWall": elemental_wall,
			}
		},
		chimera:	{
			name: "Chimera",
			stats: new Stats(1, 0, 0, 3, 1),
			actions : {
				"FeralOutbreak": feral_outbreak,
			}
		},
		ilusionist:	{
			name: "Illusionist",
			stats: new Stats(0, 0, 1, 1, 3),
		},
		demonologist:	{
			name: "Demonologist",
			stats: new Stats(0, 1, 0, 3, 1),
			actions : {
				"InfernalHand": infernal_hand,
			}
		},
		messiah:	{
			name: "Messiah",
			stats: new Stats(0, 0, 0, 3, 2),
			actions : {
				"InfiniteDivineLove": infinite_divine_love,
			}
		},
		mirror:		{
			name: "Mirror",
			stats: new Stats(0, 3, 2, 0, 0)
		},

		aaaa:	{
			name: "aaaa",
			stats: new Stats(3, 2, 0, 0, 0)
		},
		bbbb:	{
			name: "bbbb",
			stats: new Stats(3, 1, 0, 0, 1)
		},
		cccc:	{
			name: "cccc",
			stats: new Stats(0, 2, 0, 0, 3)
		},
		dddd:	{
			name: "dddd",
			stats: new Stats(2, 3, 0, 0, 0)
		},
		eeee:	{
			name: "eeee",
			stats: new Stats(3, 0, 0, 0, 2)
		},
		ffff:	{
			name: "ffff",
			stats: new Stats(0, 3, 1, 0, 1)
		},
		gggg:	{
			name: "gggg",
			stats: new Stats(1, 0, 3, 0, 1)
		},
		hhhh:	{
			name: "hhhh",
			stats: new Stats(2, 0, 1, 0, 2)
		}

	},

	motivations: ["Friends", "Money", "Family", "Vengeance", "Nation", "Knowledge", "Pleasure", "Chaos", "Power"],

}

for (key in defaults.races) {
	var obj = defaults.races[key]
	var classless = (obj.classless == true ? true : false)
	new Race( obj.name, obj.stats, classless, obj.actions )
}
//console.log(Race.objs)

for (key in defaults.classes) {
	var obj = defaults.classes[key]
	new Class( obj.name, obj.stats, obj.actions )
}
//console.log(Class.objs)

Char.motivations = defaults.motivations
//console.log(Char.motivations)

Char.actions = {
	"Hide": hide,
	"Show": show,
	"Seek Enemy": seek_enemy,

	"Punch": punch,
	"Kick": kick,
	"Kneestrike" : kneestrike,
	"HitWeakspot" : hitweakspot,

	"ArmedAttack" : armed_attack
}

//Char.attacks = []
//Char.attacks = ["Punch", "Kick", "Kneestrike", "HitWeakspot"]

/*
Char.actions = {
	build: build,
	forge: forge,
	forge_bonus_item: forge_bonus_item,

	punch: punch,
	kick: kick,
	kneestrike: kneestrike,
	weakspot: weakspot,

	run_to_mama: run_to_mama,
	seek: seek,
	show_up: show_up
}
*/

var damage_kinds = ["Impact", "Slash", "Perfuration", "Shooting", "Throwing", "Explosion", "Fire", "Water", "Air", "Earth", "Illusion", "Shadows", "Poison", "Cheese"]
var damage_obj = {}
for (var i in damage_kinds) {
	damage_obj[ damage_kinds[i] ] = 0
}
