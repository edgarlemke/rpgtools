function combat_base(char_obj, combat_obj, action_name) {
	var action_fn = char_obj.actions[action_name]

	var hability = create_select( action_fn.name + "-hability", action_fn.habilities )

	combat_fill_action(char_obj, combat_obj, action_name, [hability], "combat_base.result")
}
combat_base.result = function (action_fn_name, combat_obj_index) {
	var select = document.getElementById(action_fn_name+"-select")
	var char_d20 = document.getElementById(action_fn_name+"-char-d20")
	var enemy_d20 = document.getElementById(action_fn_name+"-enemy-d20")
	var hability = document.getElementById(action_fn_name+"-hability")

	var combat_obj = Combat.objs[ combat_obj_index ]

	var enemy_index = get_selected(select)
	var enemy_char_obj = Char.objs[enemy_index]

	var hab = get_selected(hability)

	var fn = eval(action_fn_name)
	var result = fn(combat_obj, enemy_char_obj, Number(char_d20.value), Number(enemy_d20.value), hab)
	alert(result.msg)

	update_combat_chars_status(combat_obj)
}



function combat_armed_attack (char_obj, combat_obj, action_name) {
	var action_fn = char_obj.actions[action_name]
	var weapon = char_obj.inventory.get_weapon()

	var habilities = []
	var damage_type = []
	if (action_fn.hab_dmg_from_weapon) {
		habilities = [... weapon.meta_obj.habilities]
		damage_type = [... weapon.meta_obj.damage_type]

		var select_habilities = create_select(action_fn.name + "-hability", habilities)
		var select_damage_type = create_select(action_fn.name + "-damage_type", damage_type)
	}
	else {
		var action_habilities = [...action_fn.habilities]
        	var action_damage_type = action_fn.damage_type

        	if (!weapon) {
        		var msg = char_obj.name + " has no weapon equipped!"
        
        		if (action_fn.requires_weapon) {
        			msg += "\nWeapon is required for " + action_name + "!"
        			alert(msg)
        			return false
        		}
        
        		var answer = prompt("Continue without weapon? 1 for yes, 0 for no: ")
        		if (answer != "1") {
        			return false
        		}
        
        		habilities = action_habilities
        		damage_type = action_damage_type 
        	}
        	else {
                	habilities = []
                	for (var i = 0; i < weapon.meta_obj.habilities.length; i++) {
                		var wh = weapon.meta_obj.habilities[i]
                		if (action_habilities.indexOf(wh) > -1) {
                			habilities.push(wh)
                		}
        	    	}
        
        		damage_type = []
        		for (var i = 0; i < weapon.meta_obj.damage_type.length; i++) {
        			var wd = weapon.meta_obj.damage_type[i]
        			if (action_damage_type.indexOf(wd) > -1) {
        				damage_type.push(wd)
        			}
        		}
        	}

		var select_habilities = create_select(action_fn.name + "-hability", habilities)
		var select_damage_type = create_select(action_fn.name + "-damage_type", damage_type)
	}

	combat_fill_action(char_obj, combat_obj, action_name, [select_habilities, select_damage_type], "combat_armed_attack.result")

}
combat_armed_attack.result = function (action_fn_name, combat_obj_index) {
	var select = document.getElementById(action_fn_name+"-select")
	var char_d20 = document.getElementById(action_fn_name+"-char-d20")
	var enemy_d20 = document.getElementById(action_fn_name+"-enemy-d20")
	
	var hability = document.getElementById(action_fn_name+"-hability")
	var damage_type = document.getElementById(action_fn_name+"-damage_type")

	var combat_obj = Combat.objs[ combat_obj_index ]

	var enemy_index = get_selected(select)
	var enemy_char_obj = Char.objs[enemy_index]

	var damage_type = get_selected(damage_type)
	var hability = get_selected(hability)

	var fn = eval(action_fn_name)
	var result = fn(combat_obj, enemy_char_obj, Number(char_d20.value), Number(enemy_d20.value), hability, damage_type)
	alert(result.msg)

	update_combat_chars_status(combat_obj)
}


function combat_fill_action(char_obj, combat_obj, action_name, optfields, handler) {
	var action_fn = char_obj.actions[action_name]
	var action_fn_name = action_fn.name
	var combat_obj_index = Combat.get_index(combat_obj)

	var attackable_enemies = combat_obj.get_attackable_enemies()
	//console.log(attackable_enemies)

	var action_container = document.getElementById("action-container")
	action_container.innerHTML = ""

	var select = document.createElement("select")	
	select.setAttribute("id", action_fn_name + "-select")
	for(var i = 0; i < attackable_enemies.length; i++) {
		var char_obj_index = attackable_enemies[i]
		var attackable_enemy = Char.objs[char_obj_index]

		var option = document.createElement("option")
		option.setAttribute("value", char_obj_index)
		option.innerHTML = attackable_enemy.name

		select.appendChild(option)
	}

	action_container.appendChild(select)

	var char_d20 = document.createElement("input")
	char_d20.setAttribute("id", action_fn_name + "-char-d20")
	char_d20.setAttribute("type", "number")
	action_container.appendChild(char_d20)

	var enemy_d20 = document.createElement("input")
	enemy_d20.setAttribute("id", action_fn_name + "-enemy-d20")
	enemy_d20.setAttribute("type", "number")
	action_container.appendChild(enemy_d20)


	for (var i = 0; i < optfields.length; i++) {
		action_container.appendChild(optfields[i])
	}


	var button_ok = document.createElement("input")
	button_ok.setAttribute("type", "button")
	button_ok.setAttribute("value", action_name)
	button_ok.setAttribute("onclick", handler + "('" + action_fn.name + "', " + combat_obj_index + ")")

	action_container.appendChild(button_ok)
}



function create_select (id, items) {
	var sel = document.createElement("select")
	sel.setAttribute("id", id)
	for (var i = 0; i < items.length; i++) {
		var item = items[i]
		var option = document.createElement("option")
		option.setAttribute("value", item)
		option.innerHTML = item
		sel.appendChild(option)
	}
	return sel
}
