function get_random_char () {
	var char_ = Char.get_random();

	//console.log(char_)

	update_all_chars();
}


function get_dice () {
	var input = prompt("Dice");
	alert(dice(Number(input)));
}


function get_drop_item () {
	var item = airdrop();
	var item_name = item.constructor.name;

	alert(item_name);
}

function create_new_char () {
	var name = document.getElementById("new_char_name").value;
	var age = document.getElementById("new_char_age").value;

	var race_select = document.getElementById("new_char_race");
	var race = Race.objs[get_selected(race_select)].name;

	var class_select = document.getElementById("new_char_class");
	var class_ = Class.objs[get_selected(class_select)].name;

	var motivation_select = document.getElementById("new_char_motivation");
	var motivation = defaults.motivations[ get_selected(motivation_select) ];

	var health = Number( document.getElementById("new_char_health").value );

	var player_stats = new_char_get_stats();

	//var karma_pts = new_char_get_karma_pts();
	//console.log

	var char_ = new Char(name, age, race, class_, player_stats, [], [motivation], health);

	/*
	*/

	update_all_chars();

	return false;
}

function new_char_get_stats () {
	return new Stats(
		Number(document.getElementById("new_char_stats_str").value),
		Number(document.getElementById("new_char_stats_agi").value),
		Number(document.getElementById("new_char_stats_dex").value),
		Number(document.getElementById("new_char_stats_int").value),
		Number(document.getElementById("new_char_stats_cha").value),
	);
}

/*
function new_char_get_karma_pts () {
	var karma_1_stat = get_selected( document.getElementById("karma_1_stat") );
	var karma_1_dir = get_selected( document.getElementById("karma_1_dir") );

	var karma_2_stat = get_selected( document.getElementById("karma_2_stat") );
	var karma_2_dir = get_selected( document.getElementById("karma_2_dir") );

	var karma_3_stat = get_selected( document.getElementById("karma_3_stat") );
	var karma_3_dir = get_selected( document.getElementById("karma_3_dir") );

	return [
		new Karma(Number(karma_1_stat), Number(karma_1_dir)),
		new Karma(Number(karma_2_stat), Number(karma_2_dir)),
		new Karma(Number(karma_3_stat), Number(karma_3_dir)),
	];
}
*/

function update_all_chars () {
	var all_chars_selects = document.getElementsByClassName("all_chars");

	for (var h = 0; h < all_chars_selects.length; h++) {
		var each_select = all_chars_selects[h];

		// clean old options
		for(var i = each_select.children.length - 1; i >= 0; i--) {
			each_select.removeChild( each_select.children[i] );
		}

		// add new options
		for(var ch in Char.objs) {
			var each_char = Char.objs[ch];

			var new_option = document.createElement("option");
			new_option.value = ch;
			new_option.innerHTML = each_char.name;

			each_select.appendChild(new_option);
		}
	}

}

function show_char () {
	var all_chars_select = document.getElementById("all_chars");
	var char_ = Char.objs[ get_selected(all_chars_select) ];
	
	/*
	var habkeys = Object.keys(char_.stats)
	var cvt_karma = {}
	for (var k in char_.karma) {
		var eachk = char_.karma[k]
		var hab = (habkeys[eachk.attr] !== undefined ? habkeys[eachk.attr] : "Life")
		console.log(hab)
	}
	*/

	var msg = `Char\n 
Name: ` + char_.name + `
Age: ` + char_.age + `
Race: ` + char_.race.name + `
Class: ` + char_.class_.name + `

Health: ` + char_.health + `

\t\tRace\tClass\tPlayer\tTotal
Str:\t` + char_.race.stats.Strength + `\t\t` + (char_.class_.stats !== undefined ? char_.class_.stats.Strength : 0)  + `\t\t`+ (char_.player_stats.Strength) +`\t\t` + (char_.stats.Strength) + `
Agi:\t` + char_.race.stats.Agility + `\t\t` + (char_.class_.stats !== undefined ? char_.class_.stats.Agility : 0)  + `\t\t`+ (char_.player_stats.Agility) +`\t\t` + (char_.stats.Agility) + `
Dex:\t` + char_.race.stats.Dextrity + `\t\t` + (char_.class_.stats !== undefined ? char_.class_.stats.Dextrity : 0)  + `\t\t`+ (char_.player_stats.Dextrity) +`\t\t` + (char_.stats.Dextrity) + `
Int:\t` + char_.race.stats.Intelligence + `\t\t` + (char_.class_.stats !== undefined ? char_.class_.stats.Intelligence : 0)  + `\t\t`+ (char_.player_stats.Intelligence) +`\t\t` + (char_.stats.Intelligence) + `
Cha:\t` + char_.race.stats.Charisma + `\t\t` + (char_.class_.stats !== undefined ? char_.class_.stats.Charisma : 0)  + `\t\t`+ (char_.player_stats.Charisma) +`\t\t` + (char_.stats.Charisma) + `
`
		;

	alert(msg)
}

function show_char_inventory () {
	var all_chars_select = document.getElementById("all_chars");
	var char_ = Char.objs[ get_selected(all_chars_select) ];


	var inventory_container = document.getElementById("inventory-container")
	inventory_container.innerHTML = ""

	var inventory_table = document.createElement("table")

	var thead = document.createElement("thead")
	inventory_table.appendChild(thead)

	var thead_tr = document.createElement("tr")
	thead.appendChild(thead_tr)

	var col_titles = ["Item", "Equip/Unequip", "Info"]

	for (var i = 0; i < col_titles.length; i++) {
		var title = col_titles[i]
		var th = document.createElement("th")
		th.innerHTML = title
		thead_tr.appendChild(th)
	}


	var tbody = document.createElement("tbody")
	inventory_table.appendChild(tbody)

	var coins_ct = 0
	for (var i = 0; i < char_.inventory.items.length; i++) {
		var item = char_.inventory.items[i]
		if (item.name == "Coin") {
			coins_ct += 1
			continue
		}
		
		var tr = document.createElement("tr")
		tbody.appendChild(tr)

		var item_td = document.createElement("td")
		item_td.innerHTML = item.name === undefined ? item.item_obj.name : item.name
		tr.appendChild(item_td)


		//
		var equipped = char_.inventory.equipped_items.indexOf(item) > -1 
		var equippable = item.equip_slot != null

		var equip_td = document.createElement("td")

		if (equippable) {
        		var equip_button = document.createElement("input")
        
	    		equip_button.setAttribute("type", "button")
    			equip_button.setAttribute("value", equipped ? "Unequip" : "Equip")

			var char_obj_index = Char.get_index(char_)
			equip_button.setAttribute("onclick", "inventory_toggle_equip(" + char_obj_index + ", " + i +", this)")
			equip_td.appendChild(equip_button)
		}
		tr.appendChild(equip_td)

		var info_td = document.createElement("td")

		var info_button = document.createElement("input")
		info_button.setAttribute("type", "button")
		info_button.setAttribute("value", "Info")
		info_button.setAttribute("onclick", "show_item_info(" + char_obj_index +", " + i + ")")
		info_td.appendChild(info_button)

		tr.appendChild(info_td)
	}
	var coins_div = document.createElement("div")
	coins_div.innerHTML = "<b>Coins:</b> " + coins_ct
	inventory_container.appendChild(coins_div)

	inventory_container.appendChild(inventory_table)
}

function inventory_toggle_equip (char_obj_index, item_index, button_el) {
	var char_obj = Char.objs[char_obj_index]
	var item_obj = char_obj.inventory.items[item_index]

	var equipped_index = char_obj.inventory.equipped_items.indexOf(item_obj)
	// if should unequip
	if (equipped_index > -1) {
		char_obj.inventory.equipped_items.splice(equipped_index, 1)
		button_el.value = "Equip"
	}
	// else, should equip
	else {
		// test if slot is free
		for (var i = 0; i < char_obj.inventory.equipped_items.length; i++) {
			var eq_item = char_obj.inventory.equipped_items[i]
			if (eq_item.equip_slot == item_obj.equip_slot) {
				alert("Already equipped " + eq_item.name + " as " + eq_item.equip_slot);
				return false;
			}
		}
		char_obj.inventory.equipped_items.push(item_obj)
		button_el.value = "Unequip"
	}
}

function show_item_info(char_obj_index, i) {
	var char_obj = Char.objs[char_obj_index]
	var item = char_obj.inventory.items[i]

	msg = `Name: ` + item.name + `
Category: ` + item.category + `     Slot: ` + (item.equip_slot != null ? item.equip_slot : '-') + `\n
Weight: ` + item.weight + `     Stacking: ` + item.stacking + `\n
Buildable: ` + (item.buildable ? 'Yes' : 'No') + `
Made of: \n`

	for(k in item.made_of) {
		msg += '     ' + item.made_of[k] + ' x ' + k + '\n'
	}

	msg += `Price: ` + item.price + `
Min Level: ` + item.level + `
Exclusive Use Class: ` + (item.use_class != null ? item.use_class : '-') + `
Build Class: ` + (item.build_class != null ? item.build_class : '-') + '\n\n'

	switch (item.category) {
		case 'Weapon':
			msg += `Habilities: ` + item.meta_obj.habilities.join(', ') + `
Damage Type: ` + item.meta_obj.damage_type.join(', ') + `
Ammo: ` + (item.meta_obj.ammo != null ? item.meta_obj.ammo : '-')

			break

		case 'Protection':
			var slots = [
				'AirMagicalProtection', 'EarthMagicalProtection', 'FireMagicalProtection',
				'WaterMagicalProtection', 'CheeseMagicalProtection', 'IllusionMagicalProtection',
				'ShadowsMagicalProtection'
			]
			var slot_i = slots.indexOf(item.equip_slot)

			if (slot_i > -1) {
				var dp = 30 * (item.level+1)
			}
			else {
				var dp = item.price
			}

			msg += `Damage Type: ` + item.meta_obj.damage_type.join(', ') + `
Test Points: ` + item.meta_obj.test_pts + `
Damage Protection: ` + dp

			break

		default:
			break
	}

	alert(msg)
}


function create_new_combat () {
	var alias = document.getElementById("combat-alias").value

	var teams = []
	var dice_data = []

	var combat_tables = document.getElementsByClassName("combat-team-table")
	for(var i = 0; i < combat_tables.length; i++) {
		var table = combat_tables[i]
		var tbody = table.children[0]

		var new_team = []
		var new_dice_data = []

		//console.log(table)

		for(var j = 0; j < tbody.children.length; j++) {
			var tr = tbody.children[j]
			//console.log(tr)

			var char_id = Number( tr.children[0].getAttribute("data-char-id") );
			var dice = tr.children[1].children[0].value;
			//console.log(char_id);
			//console.log(dice);

			var char_obj = Char.objs[char_id]

			new_team.push(char_obj)
			new_dice_data.push({
				"char_obj": char_obj, "d20": dice
			})
		}

		teams.push(new_team)
		dice_data.push(new_dice_data)
	}

	//console.log(teams)
	//console.log(dice_data)

	var combat = new Combat(alias, teams, dice_data)
	//console.log(combat)
	
	fill_combat_selects()
}

function fill_combat_selects () {
	var selects = document.getElementsByClassName("combat-list")
	for (var i = 0; i < selects.length; i++) {
		var sel = selects[i]

		for (var j = sel.children.length - 1; j >= 0; j--) {
			sel.removeChild(sel.children[j])
		}

		for (var j = 0; j < Combat.objs.length; j++) {
			var combat_obj = Combat.objs[j]

			var option = document.createElement("option")
			option.value = j
			option.innerHTML = combat_obj.alias

			sel.append(option)
		}
	}
}

function fill_combat_round () {
	var sel = document.getElementById("combat-round-select")
	var opt = get_selected(sel)
	var combat_obj = Combat.objs[opt]

	//console.log(combat_obj.action_order)

	var combat_round = document.getElementById("combat-round")

	var tbody = combat_round.children[1]
	for (var i = tbody.children.length - 1; i >= 0; i--) {
		tbody.removeChild(tbody.children[i])
	}

	for (var i = 0; i < combat_obj.action_order.length; i++) {
		var pair = combat_obj.action_order[i]
		var char_obj = pair[0]
		var str = pair[1]

		var tr = document.createElement("tr")
		var name_td = document.createElement("td")
		name_td.innerHTML = char_obj.name

		var order_td = document.createElement("td")
		order_td.innerHTML = str

		var status_td = document.createElement("td")
		status_td.innerHTML = stringfy_combat_slot( combat_obj, combat_obj.get_slot_index(char_obj) )

		var actions_td = document.createElement("td")

		tr.appendChild(name_td)
		tr.appendChild(order_td)
		tr.appendChild(status_td)
		tr.appendChild(actions_td)

		tbody.appendChild(tr)
	}

	fill_combat_char_actions(combat_obj)
}
function stringfy_combat_slot (combat_obj, slot_index) {
	var slot = combat_obj.slots[ slot_index ]
	var status_ = []

	var health = slot.char_obj.live.health

	status_.push(slot.active ? "Active (" + health + ")" : "Down")

	if (slot.hidden) {
		status_.push("Hidden")
	}

	var msg = ""
	for(var i = 0; i < status_.length; i++) {
		if (msg.length > 0) {
			msg += ", "
		}

		msg += status_[i]
	}

	return msg
}


function update_combat_chars_status (combat_obj) {
	var combat_round = document.getElementById("combat-round")
	var tbody = combat_round.children[1]

	for (var i = 0; i < tbody.children.length; i++) {
		var tr = tbody.children[i]
		var status_td = tr.children[2]

		var slot = combat_obj.slots[i]
		var msg = stringfy_combat_slot(combat_obj, i)

		status_td.innerHTML = msg
	}
}


function fill_combat_char_actions (combat_obj) {
	var combat_round = document.getElementById("combat-round")
	var tbody = combat_round.children[1]

	//console.log(tbody.children)

	var tr = tbody.children[ combat_obj.cursor ]
	//console.log(tr)
	
	var actions_td = tr.children[3]
	//console.log(status_td)
	
	//console.log(combat_obj)
	var pair = combat_obj.action_order[ combat_obj.cursor ]
	var char_obj = pair[0]
	var char_obj_index = Char.get_index(char_obj)

	var combat_obj_index = Combat.get_index(combat_obj)

	//console.log(char_obj)
	//console.log(char_obj.actions)
	for (var i in char_obj.actions) {
		var action = char_obj.actions[i]

		var button = document.createElement("input")
		button.setAttribute("type", "button")
		button.setAttribute("value", i)

		button.setAttribute("onclick", "combat_act(\"" + i + "\", " + char_obj_index + ", " + combat_obj_index + ")")

		actions_td.appendChild(button)
	}

	/*
	for (var i = 0; i < char_obj.attacks.length; i++) {
		var attack = char_obj.attacks[i]

		var button = document.createElement("input")
		button.setAttribute("type", "button")
		button.setAttribute("value", attack)

		actions_td.appendChild(button)
	}
	*/

	var skip_button = document.createElement("input")
	skip_button.setAttribute("type", "button")
	skip_button.setAttribute("value", "Skip")
	skip_button.setAttribute("onclick", "combat_skip(" + combat_obj_index + ")")

	actions_td.appendChild(skip_button)
}

function combat_act (action, char_obj_index, combat_obj_index) {
	var char_obj = Char.objs[ char_obj_index ]
	var combat_obj = Combat.objs[ combat_obj_index ]

	console.log(char_obj)

	var args = {char_obj: char_obj, combat_obj: combat_obj}

	switch(action) {
		case "Seek Enemy":
			combat_seek_enemy(combat_obj)
			break

		case "Punch":
		case "Kick":
		case "Kneestrike":
		case "HitWeakspot":
			combat_base(char_obj, combat_obj, action)
			break
		
		case "ArmedAttack":
		case "CheeseSpikes":
		case "FieryCreamCheese":
		case "ViolentOnslaught":
		case "BloodyJump":
		case "ShockBlade":
		case "AstralDischarge":
		case "ElementalSphere":
		case "ElementalSpikes":
		case "ElementalSpiral":
		case "ElementalWall":
		case "FeralOutbreak":
		case "InfernalHand":
		case "InfiniteDivineLove":
			combat_armed_attack(char_obj, combat_obj, action)
			break

		case "SinisterFeast":
			combat_bonus_attack(combat_obj, action)
			break

		default:
			//
			//combat_obj.act(action, args)
			console.log("Action not matched: " + action)
			return false
			break
	}

	update_combat_chars_status( combat_obj )	
}

function combat_seek_enemy (combat_obj) {
	var combat_obj_index = Combat.get_index(combat_obj)

	var hidden_enemies = combat_obj.get_hidden_enemies()
	//console.log(hidden_enemies)
	if (hidden_enemies.length == 0) {
		alert("No hidden enemies!")
		return
	}

	var action_container = document.getElementById("action-container")
	action_container.innerHTML = ""


	var select = document.createElement("select")
	select.setAttribute("id", "seek-enemy-select")
	for(var i = 0; i < hidden_enemies.length; i++) {
		var hidden_enemy = hidden_enemies[i]
		var char_obj_index = Char.get_index(hidden_enemy)

		var option = document.createElement("option")
		option.setAttribute("value", char_obj_index)
		option.innerHTML = hidden_enemy.name

		select.appendChild(option)
	}

	action_container.appendChild(select)

	var button_ok = document.createElement("input")
	button_ok.setAttribute("type", "button")
	button_ok.setAttribute("value", "Seek Enemy")
	button_ok.setAttribute("onclick", "combat_seek_enemy.result(" + combat_obj_index + ")")

	action_container.appendChild(button_ok)

}
combat_seek_enemy.result = function (combat_obj_index) {
	var combat_obj = Combat.objs[combat_obj_index]

	var select = document.getElementById("seek-enemy-select")
	var char_obj = Char.objs[ get_selected(select) ]

	var slot_index = combat_obj.get_slot_index(char_obj)
	combat_obj.slots[slot_index].hidden = false

	update_combat_chars_status(combat_obj, slot_index)

	alert("Enemy " + char_obj.name + " not hidden anymore!")
}


function combat_skip (combat_obj_index) {
	var combat_obj = Combat.objs[ combat_obj_index ]

	var combat_round = document.getElementById("combat-round")
	var tbody = combat_round.children[1]
	var tr = tbody.children[ combat_obj.cursor ]
	var actions_td = tr.children[3]
	actions_td.innerHTML = ""

	combat_obj.skip()

	fill_combat_char_actions(combat_obj)
}


function create_team_member (div) {
	var table = div.children[0];
	var select = div.children[1];

	var ch = get_selected(select);


	var tr = document.createElement("tr");
	var name_td = document.createElement("td")
	var dice_td = document.createElement("td")
	var del_td = document.createElement("td")

	var dice = document.createElement("input")
	dice.setAttribute("type", "number")
	dice.setAttribute("min", "1")
	dice.setAttribute("max", "100")
	dice_td.appendChild(dice)

	tr.appendChild(name_td);
	tr.appendChild(dice_td);
	tr.appendChild(del_td);
	table.children[0].appendChild(tr);

	name_td.innerHTML = Char.objs[ch].name;
	name_td.setAttribute("data-char-id", String(ch));

	var del_button = document.createElement("input");
	del_button.setAttribute("type", "button");
	del_button.setAttribute("value", "x");
	del_button.setAttribute("onclick", "var p = this.parentNode.parentNode; p.parentNode.removeChild(p);");

	del_td.appendChild(del_button);

}

function create_new_team () {
	var teams = document.getElementById("combat-teams");
	teams.appendChild(create_new_team.div.cloneNode(true));
	update_all_chars();
}


function export_json () {

	var obj = {
		chars: []
	};
	for(ch in Char.objs) {
		var char_ = Char.objs[ch];
		var json_ch = {
			name: char_.name,
			age: char_.age,
			health: char_.health,
			motivations: char_.motivations,

			class_: char_.class_,
			race: char_.race,

			stats: char_.stats,
			karma: char_.karma,
		};

		obj.chars.push(json_ch);
	}


	var data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(obj));
	var anchor = document.getElementById("jsonanchor");

	anchor.setAttribute("href", data);
	anchor.setAttribute("download", "rpgtools.js");
	anchor.click();
}

function import_json () {
	var jsontext = document.getElementById("jsontext").value;
	var obj = JSON.parse(jsontext);
	console.log(obj);

	for(ch in obj.chars){
		var each_char = obj.chars[ch];
		var CH = new Char(
			each_char.name, each_char.age, each_char.race.name, each_char.class_.name, new Stats(0,0,0,0,0) /*each_char.stats*/, each_char.karma
		);
		CH.stats = each_char.stats;
		CH.health = each_char.health;
		console.log(CH);
	}
}


// Setup function
(function () {
	fill_race_select();
	fill_class_select();
	fill_motivation_select();
	fill_stat_selects();

	create_new_team.div = document.getElementsByClassName("combat-team")[0].cloneNode(true);

	/*
	function sleep () {
		return new Promise (resolve => setTimeout(resolve, 1000))
	}

	var all = {}

	async function _x_ () {
		for ( var c = 0; c < 100; c++ )	{
			var char_ = Char.get_random()
	
			var key = char_.race.name
	
			if (char_.class_ != false) {
				key = char_.class_.name
			}
			
			if (Object.keys(all).indexOf(key) == -1) {
				all[key] = 0
			}
	
			all[key] += 1
	
			console.log(".")
			await sleep()
		}
		//console.log(all)
		var sorted = Object.keys(all).sort(function(a,b){return all[a]-all[b]})
		for(var t in sorted) {
			var s = sorted[t]
			console.log(s + " - " + all[s])
		}
	
	}

	_x_()
	*/

	function get_total (src) {
		total = {}
		for (var r in src)	{
			var src_obj = src[r]
			for (var hab in src_obj.stats) {
				if (Object.keys(total).indexOf(hab) == -1) {
					total[hab] = 0
				}
	
				total[hab] += src_obj.stats[hab]
			}
		}
		return total
	}
	//console.log(get_total(Race.objs))
	//console.log(get_total(Class.objs))
	

})();


function fill_race_select() {
	var race_select = document.getElementById("new_char_race");
	for(var i = 0; i < Race.objs.length; i++) {
		var each_race = Race.objs[i];
		
		var new_option = document.createElement("option");
		new_option.value = i;
		new_option.innerHTML = each_race.name;

		race_select.appendChild(new_option);
	}
}

function fill_class_select () {
	var class_select = document.getElementById("new_char_class");
	for(var i = 0; i < Class.objs.length; i++) {
		var each_class = Class.objs[i];

		var new_option = document.createElement("option");
		new_option.value = i;
		new_option.innerHTML = each_class.name;

		class_select.appendChild(new_option);
	}
}

function fill_motivation_select () {
	var m_select = document.getElementById("new_char_motivation");
	for(m in defaults.motivations) {
		var each_m = defaults.motivations[m];
		
		var new_option = document.createElement("option");
		new_option.value = m;
		new_option.innerHTML = each_m;

		m_select.appendChild(new_option);
	}
}

function fill_stat_selects () {
	var s_ = {
		1 : "Strength",
		2 : "Agility" , 
		3 : "Dextrity" ,
		4 : "Intelligence" ,
		5 : "Charisma",
		6 : "Health"
	}

	var stat_selects = document.getElementsByClassName("stat_select");
	for(var i = 0; i < stat_selects.length; i++){
		var each_select = stat_selects[i];

		for(var j = 1; j <= Object.keys(s_).length; j++) {
			var each_stat = s_[j];

			var new_option = document.createElement("option");
			new_option.value = j;
			new_option.innerHTML = each_stat;

			each_select.appendChild(new_option);
		}
	}
}


function get_selected(select) {
	for(var i = 0; i < select.children.length; i++)	{
		var each_child = select.children[i];
		if(each_child.selected)
			return each_child.value;
	}
}
