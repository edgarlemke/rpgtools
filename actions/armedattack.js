function armed_attack (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 7, true, true)
}

armed_attack.requires_weapon = true
armed_attack.hab_dmg_from_weapon = true
