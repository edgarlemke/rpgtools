function hitweakspot (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, "Impact", 13, true, false)
}

hitweakspot.habilities = ["Strength", "Dextrity", "Agility"]
