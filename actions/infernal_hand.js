function infernal_hand (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 13, false, true)
}

infernal_hand.habilities = ["Intelligence"]
infernal_hand.damage_type = ["Shadows"]
infernal_hand.requires_weapon = true
