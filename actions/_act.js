function _act (action, char_obj_index, combat_obj_index) {
	var char_obj = Char.objs[ char_obj_index ]
	var combat_obj = Combat.objs[ combat_obj_index ]

	for(var i = 0; i < _actions.length; i++) {
		var action_fn = _actions[i]

		if (action_fn.action_name != action)
			continue

		action_fn(char_obj, combat_obj)
		break
	}
}
_actions = []
