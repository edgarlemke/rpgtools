function direct_attack (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, difficulty, must_show, use_weapon) {
	var char_obj = combat_obj.slots[ combat_obj.cursor ].char_obj

	var char_hability = char_obj.stats[hability]
	var enemy_hability = enemy_char_obj.stats[hability]

	var enemy_protection = direct_attack.get_enemy_protection(enemy_char_obj, damage_type)
	var char_weapon_damage = use_weapon ? direct_attack.get_weapon_damage(char_obj) : 0

	var aptitude = damage_type
	var char_aptitude = char_obj.aptitudes[aptitude]
	var enemy_resistence = enemy_char_obj.resistences[aptitude]

	var test_msg = "TEST:\n(HAB + APT + D20) - (HAB + PROT + RES) >= DIF\n(" + char_hability + " + " + char_aptitude + " + " + char_d20 + ") - (" + enemy_hability + " + " + enemy_protection.test_pts + " + " + enemy_resistence + ") >= " + difficulty + "\n\n"
	
	var hidden_msg = ""
	var slot_index = combat_obj.get_slot_index(char_obj)
	var slot = combat_obj.slots[slot_index]
	if (must_show && slot.hidden == true) {
		slot.hidden = false
		hidden_msg = char_obj.name + " not hidden anymore!\n"
	}

	if ( ((char_hability + char_aptitude + char_d20) - (enemy_hability + enemy_protection + enemy_resistence)) < difficulty ) {
		return {
			passed: false,
			damage: 0,
			msg: "FAILED!\n" + test_msg + hidden_msg
		}
	}

	var mul = (char_d20 == 20 ? 3 : 1)
	var damage = ( (((char_hability + difficulty) * 2 + char_weapon_damage) + (char_aptitude * 3)) * mul ) - (enemy_protection.life_pts + (enemy_resistence * 3))

	// subtract damage from life points of enemy
	enemy_char_obj.live.health -= damage
	if (enemy_char_obj.live.health < 0) {
		enemy_char_obj.live.health = 0
	}

	var damage_msg = `DAMAGE:
(((((HAB + DIF) * 2) + WEAPONDMG) + (APT * 3)) * MUL) - (PROTPTS + (RES * 3))
(((((` + char_hability + " + " + difficulty + ") * 2) + " + char_weapon_damage + ") + (" + char_aptitude + " * 3)) * " + mul +") - (" + enemy_protection.life_pts + " + (" + enemy_resistence + " * 3))\n\n  >> " + damage + "\n\n"
	var died_msg = ""
	if (enemy_char_obj.live.health == 0) {
		var enemy_slot_index = combat_obj.get_slot_index(enemy_char_obj)
		combat_obj.slots[enemy_slot_index].active = false
		died_msg = enemy_char_obj.name + " died! x.x\n"
	}

	// test for aptitude, add aptitude
	var d20 = Number( prompt("D20 for " + char_obj.name + " aptitude in " + aptitude + " damage") )
	var apt_msg = ""
	if (d20 >= 17) {
		char_obj.aptitudes[aptitude] += 1
		apt_msg += char_obj.name + " earned an aptitude point in " + aptitude + "\n"
	}

	// test for resistence, add resistence
	var d20 = Number( prompt("D20 for " + enemy_char_obj.name + " resistence against " + aptitude + " damage") )
	var res_msg = ""
	if (d20 >= 17) {
		enemy_char_obj.resistences[aptitude] += 1
		res_msg += enemy_char_obj.name + " earned a resistence point against " + aptitude + "\n"
	}

	return {
		passed: true,
		damage: damage,
		msg: "PASSED!\n" + test_msg + damage_msg + died_msg + apt_msg + res_msg + hidden_msg
	}

}

direct_attack.get_enemy_protection = function (enemy_char_obj, damage_type) {
	var test_pts = 0
	var life_pts = 0
	for (var i = 0; i < enemy_char_obj.inventory.equipped_items.length; i++) {
		var item = enemy_char_obj.inventory.equipped_items[i]

		if (item.category != "Protection") {
			continue
		}

		if (item.meta_obj.damage_type.indexOf(damage_type) == -1) {
			continue
		}

		test_pts += item.meta_obj.test_pts
		life_pts += item.price
	}

	return {test_pts, life_pts}
}

direct_attack.get_weapon_damage = function (char_obj) {
	for (var i = 0; i < char_obj.inventory.equipped_items.length; i++) {
		var item = char_obj.inventory.equipped_items[i]

		if (item.category != "Weapon") {
			continue
		}

		return item.price
	}

	msg = char_obj.name + " has no equipped weapon!"
	alert(msg)
	throw msg
}
