function cheese_spikes (combat_obj, enemy_char_obj, char_d20, enemy_d20) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, "Intelligence", "Cheese", 9, false, true)   
}

cheese_spikes.habilities = ["Intelligence"]
cheese_spikes.damage_type = ["Cheese"]
cheese_spikes.requires_weapon = false
