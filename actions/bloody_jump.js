function bloody_jump (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 13, true, true)
}

bloody_jump.habilities = ["Strength", "Dextrity"]
bloody_jump.damage_type = ["Impact", "Slash", "Perfuration"]
bloody_jump.requires_weapon = true
