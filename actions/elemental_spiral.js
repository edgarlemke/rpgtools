function elemental_spiral (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 13, false, true)
}

elemental_spiral.habilities = ["Intelligence"]
elemental_spiral.damage_type = ["Fire", "Air", "Earth", "Water"]
elemental_spiral.requires_weapon = false
