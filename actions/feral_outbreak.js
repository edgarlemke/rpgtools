function feral_outbreak (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 11, true, true)
}

feral_outbreak.habilities = ["Strength"]
feral_outbreak.damage_type = ["Impact", "Slash", "Perfuration"]
feral_outbreak.requires_weapon = false
