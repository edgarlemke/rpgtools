function shock_blade (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 9, false, true)
}

shock_blade.habilities = ["Dextrity"]
shock_blade.damage_type = ["Slash"]
shock_blade.requires_weapon = true
