function infinite_divine_love (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 13, true, true)
}

infinite_divine_love.habilities = ["Intelligence"]
infinite_divine_love.damage_type = ["Illusion"]
infinite_divine_love.requires_weapon = true
