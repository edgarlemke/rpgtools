function elemental_spikes (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 11, false, true)
}

elemental_spikes.habilities = ["Intelligence"]
elemental_spikes.damage_type = ["Fire", "Air", "Earth", "Water"]
elemental_spikes.requires_weapon = false
