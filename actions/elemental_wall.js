function elemental_wall (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 15, false, true)
}

elemental_wall.habilities = ["Intelligence"]
elemental_wall.damage_type = ["Fire", "Air", "Earth", "Water"]
elemental_wall.requires_weapon = false
