function violent_onslaught (combat_obj, enemy_char_obj, char_d20, enemy_d20) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, "Strength", "Impact", 11, true, true)
}

violent_onslaught.habilities = ["Strength"]
violent_onslaught.damage_type = ["Impact"]
violent_onslaught.requires_weapon = true
