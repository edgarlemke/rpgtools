function elemental_sphere (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 9, false, true)
}

elemental_sphere.habilities = ["Intelligence"]
elemental_sphere.damage_type = ["Fire", "Air", "Earth", "Water"]
elemental_sphere.requires_weapon = false
