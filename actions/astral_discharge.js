function astral_discharge (combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, hability, damage_type, 11, true, true)
}

astral_discharge.habilities = ["Intelligence"]
astral_discharge.damage_type = ["Fire", "Air", "Earth", "Water"]
astral_discharge.requires_weapon = false
