function fiery_creamcheese (combat_obj, enemy_char_obj, char_d20, enemy_d20) {
	return direct_attack(combat_obj, enemy_char_obj, char_d20, enemy_d20, "Intelligence", "Cheese", 11, false, true)
}

fiery_creamcheese.habilities = ["Intelligence"]
fiery_creamcheese.damage_type = ["Cheese"]
fiery_creamcheese.requires_weapon = false
