function _is_valid (obj, name) {
	for (var key in obj) {
		if (obj[key].name == name)
			return true
	}
	return false
}

function dice (sides= 6) {
	return Math.floor( Math.random() * (sides) ) + 1
}


class Stats {
	Strength = 0
	Agility = 0
	Dextrity = 0
	Intelligence = 0
	Charisma = 0

	constructor (str, agi, dex, int_, cha) {
		this.Strength = str
		this.Agility = agi
		this.Dextrity = dex
		this.Intelligence = int_
		this.Charisma = cha
	}

	static get_random () {
		// get random stats
		var stats = new Stats(0,0,0,0,0)
		var stats_attr = []
		for (var attr in stats)	{
			stats_attr.push(attr)
		}
		var ct = 0
		while(ct<5)	{
			var attr = stats_attr[ dice(stats_attr.length) - 1]
			stats[attr] += 1
			ct++
		}

		return stats
	}
}

const SUCCESS = 3
const FAILURE = 2
const CRITICAL = 1

