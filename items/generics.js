I("Coin",	"Generic", 1, 50,	false, null, 1, 0, null, null, null)

I("Material",	"Generic", 1, 5,	false, null, 1, 0, null, null, null)
I("Wood",	"Generic", 1, 3, 	false, null, 2, 0, null, null, null)
I("Metal",	"Generic", 1, 3,	false, null, 3, 0, null, null, null)
I("Electronics","Generic", 1, 5,	false, null, 4, 0, null, null, null)

I("Pocket",	"Generic", null, null,	false, null, 15, 0, null, null, null)

//I("ElementalGem", "Generic", 3, 1,	false, null, 100, 0, null, null, null)

I("StrengthGem",  "Generic", 3, 1,	false, null, 100, 0, null, null, "RingPlace")
I("AgilityGem",  "Generic", 3, 1,	false, null, 100, 0, null, null, "RingPlace")
I("DextrityGem",  "Generic", 3, 1,	false, null, 100, 0, null, null, "RingPlace")
I("IntelligenceGem",  "Generic", 3, 1,	false, null, 100, 0, null, null, "RingPlace")
I("CharismaGem",  "Generic", 3, 1,	false, null, 100, 0, null, null, "RingPlace")

I("StrengthRing","Generic", 1, 3,	true, {"StrengthGem": 1}, 33, 0, null, null, "RingPlace")
I("AgilityRing","Generic", 1, 3,	true, {"AgilityGem": 1}, 33, 0, null, null, "RingPlace")
I("DextrityRing","Generic", 1, 3,	true, {"DextrityGem": 1}, 33, 0, null, null, "RingPlace")
I("IntelligenceRing","Generic", 1, 3,	true, {"IntelligenceGem": 1}, 33, 0, null, null, "RingPlace")
I("CharismaRing","Generic", 1, 3,	true, {"CharismaGem": 1}, 33, 0, null, null, "RingPlace")

I("Cartucho",	"Generic", 1, 5,	false, null, 2, 0, null, null, null)
I("Projectile",	"Generic", 1, 5,	false, null, 1, 0, null, null, null)
I("GunPowder",	"Generic", 1, 3,	false, null, 2, 0, null, null, null)

I("FornoForja", "Generic", 7, 1,	false, null, 40, 0, null, null, null)

I("GarrafaComb", "Generic", 3, 1,	false, null, 12, 0, null, null, null)
I("Garrafa",	"Generic", 2, 1,	false, null, 8, 0, null, null, null)

I("WitchScript", "Generic", 1, 1,	false, null, 100, 0, null, null, null)
I("PotionMat",	"Generic", 1, 10,	false, null, 30, 0, null, null, null)

I("Flashlight",	"Generic", 1, 1,	false, null, 20, 0, null, null, null)
I("Rope",	"Generic", 5, 1,	false, null, 30, 0, null, null, null)
