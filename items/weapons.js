ww = []
function W(item_obj, habilities, damage_type, ammo)	{
	var obj = {
		"item_obj": item_obj,
		"habilities": habilities,
		"damage_type": damage_type,
		"ammo": ammo,
	}

	get_weight_price(item_obj)

	obj.damage = item_obj.price

	item_obj.meta_obj = obj
	
	ww.push(obj)
	return obj
}

//
W( 	I("ParSocoIngles", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity"], ["Impact"], null	)

W( 	I("Tonfa", "Weapon", null, 1, true, {"Wood": 4, "Material": 1, "Metal": 0, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity", "Agility"], ["Impact"], null	)

W( 	I("Bastão", "Weapon", null, 1, true, {"Wood": 5, "Material": 1, "Metal": 0, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity", "Agility"], ["Impact"], null	)

W( 	I("Corrente", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 4, "Electronics": 0}, null, 0, ["Fighter"], "Enginner", "Weapon"),
	["Strength", "Dextrity"], ["Impact"], null	)

W( 	I("ClavaDeMadeira", "Weapon", null, 1, true, {"Wood": 7, "Material": 1, "Metal": 0, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength"], ["Impact"], null	)


//
W( 	I("Navalha", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Slash"], null	)

W( 	I("FacaCaseira", "Weapon", null, 1, true, {"Wood": 1, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Perfuration", "Slash"], null	)

W( 	I("FacaDeSobrevivência", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Perfuration", "Slash"], null	)

W( 	I("PequenaFoiceAgrícola", "Weapon", null, 1, true, {"Wood": 0, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity", "Agility"], ["Slash"], null	)

W( 	I("Machete", "Weapon", null, 1, true, {"Wood": 1, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity", "Agility"], ["Slash"], null	)

W( 	I("Alfange", "Weapon", null, 1, true, {"Wood": 2, "Material": 1, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Perfuration", "Slash"], null	)

W( 	I("Cimitarra", "Weapon", null, 1, true, {"Wood": 1, "Material": 1, "Metal": 4, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Slash"], null	)


//
W( 	I("Lança", "Weapon", null, 1, true, {"Wood": 4, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Strength", "Dextrity"], ["Slash", "Throwing"], null	)

//
W( 	I("Bumerangue", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity"], ["Throwing"], null	)

W( 	I("Funda", "Weapon", null, 1, true, {"Wood": 0, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity"], ["Throwing"], null	)

W( 	I("Shuriken", "Weapon", null, 1, true, {"Wood": 0, "Material": 0, "Metal": 5, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity"], ["Perfuration", "Throwing"], null	)


//
W( 	I("Sarabatana", "Weapon", null, 1, true, {"Wood": 3, "Material": 1, "Metal": 0, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity"], ["Shooting"], "Dardo"	)

W( 	I("Arco", "Weapon", null, 1, true, {"Wood": 4, "Material": 2, "Metal": 0, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Shooting"], "Flecha"	)

W( 	I("Balestra", "Weapon", null, 1, true, {"Wood": 2, "Material": 3, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity"], ["Shooting"], "Flecha"	)

W( 	I("Garrucha", "Weapon", null, 1, true, {"Wood": 2, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Dextrity", "Agility"], ["Shooting"], "Chumbinho"	)


//
W( 	I("Amuleto", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Witch", "Weapon"),
	["Intelligence", "Charisma"], ["Fire", "Water", "Air", "Earth"], null	)

W( 	I("Relíquia", "Weapon", null, 1, true, {"Wood": 0, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Witch", "Weapon"),
	["Intelligence", "Charisma"], ["Fire", "Water", "Air", "Earth"], null	)

W( 	I("Varinha", "Weapon", null, 1, true, {"Wood": 4, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Witch", "Weapon"),
	["Intelligence", "Charisma"], ["Fire", "Water", "Air", "Earth"], null	)


//
W( 	I("BaralhoDeCartas", "Weapon", null, 1, true, {"Wood": 0, "Material": 7, "Metal": 0, "Electronics": 0}, null, 0, null, "Illusionist", "Weapon"),
	["Charisma"], ["Illusion"], null	)

W( 	I("RelógioHipnótico", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 3, "Electronics": 0}, null, 0, null, "Illusionist", "Weapon"),
	["Charisma"], ["Illusion"], null	)

W( 	I("Cartola", "Weapon", null, 1, true, {"Wood": 1, "Material": 2, "Metal": 3, "Electronics": 0}, null, 0, null, "Illusionist", "Weapon"),
	["Charisma"], ["Illusion"], null	)

W( 	I("Máscara", "Weapon", null, 1, true, {"Wood": 1, "Material": 1, "Metal": 4, "Electronics": 0}, null, 0, null, "Illusionist", "Weapon"),
	["Charisma"], ["Illusion"], null	)


//
W( 	I("Ralador", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Intelligence", "Charisma"], ["Cheese"], null	)

W( 	I("Bisnaga", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 2, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Intelligence", "Charisma"], ["Cheese"], null	)

W( 	I("Leiteira", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 3, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Intelligence", "Charisma"], ["Cheese"], null	)

W( 	I("MáquinaDeQueijo", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 4, "Electronics": 0}, null, 0, null, "Enginner", "Weapon"),
	["Intelligence", "Charisma"], ["Cheese"], null	)


//
W( 	I("CalendárioMaia", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Messiah", "Weapon"),
	["Intelligence", "Charisma"], ["Illusion"], null	)

W( 	I("LivroSagrado", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 2, "Electronics": 0}, null, 0, null, "Messiah", "Weapon"),
	["Intelligence", "Charisma"], ["Illusion"], null	)

W( 	I("TabuetasDivinas", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 3, "Electronics": 0}, null, 0, null, "Messiah", "Weapon"),
	["Intelligence", "Charisma"], ["Illusion"], null	)

W( 	I("PingenteMessiânico", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 4, "Electronics": 0}, null, 0, null, "Messiah", "Weapon"),
	["Intelligence", "Charisma"], ["Illusion"], null	)

//
W( 	I("SeloDemoníaco", "Weapon", null, 1, true, {"Wood": 0, "Material": 1, "Metal": 2, "Electronics": 0}, null, 0, null, "Demonologist", "Weapon"),
	["Intelligence", "Charisma"], ["Shadows"], null	)

W( 	I("LivretoDemoníaco", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 2, "Electronics": 0}, null, 0, null, "Demonologist", "Weapon"),
	["Intelligence", "Charisma"], ["Shadows"], null	)

W( 	I("GrimórioMenor", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 3, "Electronics": 0}, null, 0, null, "Demonologist", "Weapon"),
	["Intelligence", "Charisma"], ["Shadows"], null	)

W( 	I("Necronomicon", "Weapon", null, 1, true, {"Wood": 0, "Material": 3, "Metal": 4, "Electronics": 0}, null, 0, null, "Demonologist", "Weapon"),
	["Intelligence", "Charisma"], ["Shadows"], null	)


//console.log(ww)
