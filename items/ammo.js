var ammo_objs = []

function ammo__ (item_obj) {
	var obj = {
		"item_obj": item_obj
	}

	get_weight_price(item_obj)

	item_obj.meta_obj = obj

	ammo_objs.push(obj)
	return obj
}

function get_ammo_spending (weapon) {
	var shooting = weapon.damage_type.indexOf("Shooting") > -1
	var all_ammo = []
	var spent = 0
	if (shooting) {
		var max_spending = 20 + (dice(3) * 10)
		var ammo_obj = find_item_obj( weapon.ammo )

		while((spent+ammo_obj.price) < max_spending) {
			all_ammo.push( Object.assign({},ammo_obj) )
			spent += ammo_obj.price
		}
	}

	return [spent, all_ammo]
}


ammo__(I("Dardo", "Ammo", null, 5, true, {"Material": 1}, null, 0, null, null, null))
ammo__(I("Flecha", "Ammo", null, 5, true, {"Wood": 1}, null, 0, null, null, null))
ammo__(I("Chumbinho", "Ammo", null, 5, true, {"Metal": 1, "GunPowder": 1}, null, 0, null, null, null))
