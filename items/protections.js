pp = []
function P (item_obj, damage_type, test_pts) {
	var obj = {
		"item_obj": item_obj,
		"damage_type": damage_type,
		"test_pts": test_pts,
	}

	get_weight_price(item_obj)

	obj.protection = item_obj.price

	item_obj.meta_obj = obj

	pp.push(obj)
	return obj
}

P(	I("EscudoDeMadeira", "Protection", null, 1, true, {"Wood": 1, "Material": 1}, null, 0, null, "Engineer", "Shield"),
	["Perfuration", "Slash", "Impact", "Shooting", "Throwing", "Explosion", "Acid"],	1	)  

P(	I("CotaDeMalha", "Protection", null, 1, true, {"Material": 2, "Metal": 1}, null, 0, null, "Engineer", "ChestProtection"),
	["Perfuration", "Slash", "Impact", "Shooting", "Throwing", "Explosion", "Acid"],	2	)  

P(	I("Elmo", "Protection", null, 1, true, {"Metal": 1}, null, 0, null, "Engineer", "HeadProtection"),
	["Perfuration", "Slash", "Impact", "Shooting", "Throwing", "Explosion", "Acid"],	1	)  

P(	I("BotasSimples", "Protection", null, 1, true, {"Material": 2}, null, 0, null, "Engineer", "FeetProtection"),
	["Perfuration", "Slash", "Impact", "Shooting", "Throwing", "Explosion", "Acid"],	1	)  

P(	I("LuvasSimples", "Protection", null, 1, true, {"Material": 2}, null, 0, null, "Engineer", "HandsProtection"),
	["Perfuration", "Slash", "Impact", "Shooting", "Throwing", "Explosion", "Acid"],	1	)  


P(	I("AmuletoFogo", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "FireMagicalProtection"),
	["Fire"],	1	)

P(	I("AmuletoÁgua", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "WaterMagicalProtection"),
	["Water"],	1	) 

P(	I("AmuletoAr", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "AirMagicalProtection"),
	["Air"],	1	) 

P(	I("AmuletoTerra", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "EarthMagicalProtection"),
	["Earth"],	1	) 

P(	I("AmuletoQueijo", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "CheeseMagicalProtection"),
	["Cheese"],	1	) 

P(	I("AmuletoIlusão", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "IllusionMagicalProtection"),
	["Illusion"],	1	) 

P(	I("AmuletoTrevas", "Protection", null, 1, true, {"Wood": 1}, null, 0, null, "Witch", "ShadowsMagicalProtection"),
	["Shadows"],	1	) 

