class Class {
	static objs = []

	constructor (name, stats, actions) {
		this.name = name
		this.stats = stats
		this.actions = actions

		Class.objs.push(this)
	}

	static is_valid (name) {
		return _is_valid(Class.objs, name)
	}

	static get_obj (name) {
		for (key in Class.objs) {
			if (Class.objs[key].name == name)
				return Class.objs[key]
		}
		return false
	}

	static _last_classes = []
	static get_random () {
		// remove some races when last races have all races
		if (Class._last_classes.length == Class.objs.length) {
			var factor = Math.floor(Class.objs.length/3)
			var offset = dice(factor)-1
			var qt = dice(factor)
			Class._last_classes.splice(offset,qt)
		}

		while (true) {
			var class_ = Class.objs[ dice(Class.objs.length)-1 ].name
			if (Class._last_classes.indexOf(class_) == -1) {
				break
			}
		}

		Class._last_classes.push(class_)
		return class_
	}
}
